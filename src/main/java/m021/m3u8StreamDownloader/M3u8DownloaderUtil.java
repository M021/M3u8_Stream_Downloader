package m021.m3u8StreamDownloader;

import m021.m3u8StreamDownloader.exception.HttpServiceException;
import m021.m3u8StreamDownloader.http.HttpService;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class M3u8DownloaderUtil {

    private final static Logger logger = Logger.getLogger(M3u8DownloaderUtil.class);

    protected HttpService httpService;

    private static final String TS_DATE_LOCATOR = "#EXT-X-PROGRAM-DATE-TIME:";

    private static final int TS_BUFFER_SIZE = 5 * 1024 * 1024;

    public M3u8DownloaderUtil(HttpService httpService) {
        if (httpService == null) {
            throw new IllegalArgumentException();
        }
        this.httpService = httpService;
    }

    public List<TsFileInfo> getTsFileName(String m3u8Url) throws IOException, HttpServiceException, ParseException {
        logger.info("Start to connect to url: " + m3u8Url);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try (CloseableHttpResponse response = httpService.executeGet(new URL(m3u8Url))){
            InputStream is = response.getEntity().getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            List<TsFileInfo> tsFileInfoList = new ArrayList<>();
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(TS_DATE_LOCATOR)) {
                    /*
                    * #EXT-X-PROGRAM-DATE-TIME:2018-06-30T15:09:54Z
                      #EXTINF:10,
                      xxxxx-3-153037135.hls.ts
                      */
                    Long timeInMs = sdf.parse(line.substring(TS_DATE_LOCATOR.length())).getTime();
                    reader.readLine();
                    String filename = reader.readLine();
                    tsFileInfoList.add(new TsFileInfo(timeInMs, filename, TS_BUFFER_SIZE));
                }
            }
            return tsFileInfoList;
        }
    }

    public void downloadFile(String tsUrl, String saveToPath ) throws InterruptedException, IOException, HttpServiceException {
        logger.info("Start to download files - url: " + tsUrl + ", saveToPath: " + saveToPath);

        try (CloseableHttpResponse response = httpService.executeGet(new URL(tsUrl));
             InputStream is = response.getEntity().getContent();
             BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(saveToPath))) {

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                bos.write(buffer, 0, bytesRead);
            }
        } catch (Exception e) {
            logger.error(e, e);
            throw e;
        }
    }

    public void downloadFile(String tsUrl, OutputStream  os) throws InterruptedException, IOException, HttpServiceException {
        logger.info("Start to download files - url: " + tsUrl);

        try (CloseableHttpResponse response = httpService.executeGet(new URL(tsUrl));
             InputStream is = response.getEntity().getContent()) {
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
        } catch (Exception e) {
            logger.error(e, e);
            throw e;
        }
    }


}
