package m021.m3u8StreamDownloader;

import java.io.ByteArrayOutputStream;

public class TsFileInfo {

    private long timeInMs;

    private String filename;

    private ByteArrayOutputStream os;

    private  DownloadStatus status;

    public TsFileInfo() {
    }

    public TsFileInfo(long timeInMs, String filename, int bufferSize) {
        this.timeInMs = timeInMs;
        this.filename = filename;
        this.status = DownloadStatus.INIT;
        os = new ByteArrayOutputStream(bufferSize);
    }

    public long getTimeInMs() {
        return timeInMs;
    }

    public void setTimeInMs(long timeInMs) {
        this.timeInMs = timeInMs;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public ByteArrayOutputStream getByteArrayOutputStream() {
        return os;
    }

    public DownloadStatus getStatus() {
        return status;
    }

    public void setStatus(DownloadStatus status) {
        this.status = status;
    }
}
