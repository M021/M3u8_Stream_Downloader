package m021.m3u8StreamDownloader.http;

import org.apache.commons.lang3.ArrayUtils;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;


public class DelegateTrustManager implements X509TrustManager {

    private final X509TrustManager mainTrustManager;

    private final X509TrustManager defaultTrustManager;

    public DelegateTrustManager(X509TrustManager mainTrustManager, X509TrustManager defaultTrustManager) {
        this.mainTrustManager = mainTrustManager;
        this.defaultTrustManager = defaultTrustManager;
    }

    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
        try {
            mainTrustManager.checkClientTrusted(x509Certificates, s);
        } catch (Exception ignore) {
            defaultTrustManager.checkClientTrusted(x509Certificates, s);
        }
    }

    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
        try {
            mainTrustManager.checkServerTrusted(x509Certificates, s);
        } catch (Exception ignore) {
            defaultTrustManager.checkServerTrusted(x509Certificates, s);
        }
    }

    public X509Certificate[] getAcceptedIssuers() {
        X509Certificate[] a = mainTrustManager.getAcceptedIssuers();
        X509Certificate[] b = defaultTrustManager.getAcceptedIssuers();
        X509Certificate[] c = ArrayUtils.addAll(a, b);
        return c;
    }

}
