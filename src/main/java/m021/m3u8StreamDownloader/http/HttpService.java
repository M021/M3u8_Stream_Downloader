package m021.m3u8StreamDownloader.http;


import m021.m3u8StreamDownloader.exception.HttpServiceException;
import org.apache.http.client.methods.CloseableHttpResponse;

import java.io.IOException;
import java.net.URL;

public interface HttpService {

     CloseableHttpResponse executeGet(URL url) throws HttpServiceException, IOException;
}
