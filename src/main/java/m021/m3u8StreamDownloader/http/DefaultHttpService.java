package m021.m3u8StreamDownloader.http;

import m021.m3u8StreamDownloader.exception.HttpClientException;
import m021.m3u8StreamDownloader.exception.HttpServiceException;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;

public class DefaultHttpService implements HttpService{

    private final static Logger logger = Logger.getLogger(DefaultHttpService.class);

    protected CloseableHttpClient httpClient;

    public DefaultHttpService() throws HttpClientException {
        this.httpClient = HttpClientFactory.getHttpClient();
    }

    private void setHeader(HttpRequestBase base) {
        base.setHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36");
        base.setHeader(HttpHeaders.ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        base.setHeader(HttpHeaders.ACCEPT_ENCODING, "gzip, deflate, br");
        base.setHeader(HttpHeaders.ACCEPT_LANGUAGE, "zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4");
        base.setHeader("Upgrade-Insecure-Requests", "1");
    }

    public CloseableHttpResponse executeGet(URL url) throws HttpServiceException, IOException {
        HttpGet httpGet = new HttpGet(url.toString());
        setHeader(httpGet);
        CloseableHttpResponse response;

        logger.debug("url : " + url.toString());
        try {
            response = httpClient.execute(httpGet);
        } catch (IOException e) {
            logger.error("Failed to get the response -  url: " + httpGet.getURI(), e.getCause());
            throw new HttpServiceException(e.getMessage(), e.getCause());
        }

        validateStatusCode(response, httpGet.getURI().toString());

        if (response.getEntity() == null) {
            throw new HttpServiceException("The entity is null - url: " + httpGet.getURI());
        }
        return response;
    }

    private void validateStatusCode(HttpResponse response, String url) throws HttpServiceException {
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != HttpStatus.SC_OK) {
            String errMsg = "The response status code is not valid. -  url: " + url + " statusCode: " + statusCode;
            throw new HttpServiceException(errMsg);
        }
    }



}
