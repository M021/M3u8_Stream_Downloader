package m021.m3u8StreamDownloader;

import m021.m3u8StreamDownloader.exception.HttpClientException;
import m021.m3u8StreamDownloader.http.DefaultHttpService;
import m021.m3u8StreamDownloader.exception.HttpServiceException;
import org.apache.log4j.Logger;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class M3u8StreamDownloader {

    private final static Logger logger = Logger.getLogger(M3u8StreamDownloader.class);

    private static ExecutorService executorService = Executors.newFixedThreadPool(30);

    private static String M3U8_URL;
    private static String TS_DOWNLOAD_URL;
    private static String TS_SAVE_TO_PATH;
    private static int SKIP_WHEN_NUM_OF_TS_READY = 3;

    public static void main(String[] args) throws HttpClientException, IOException, HttpServiceException, InterruptedException, ParseException {

        if (args.length != 3) {
            System.out.println("[Error]: Incorrect command.");
            System.out.println("[Usage]: java -jar m3u8StreamDownloader.jar <M3U8_URL> <TS_DOWNLOAD_URL> <TS_SAVE_TO_PATH>");
            return;
        }
        M3U8_URL = args[0];
        TS_DOWNLOAD_URL = args[1];
        TS_SAVE_TO_PATH = args[2];

        M3u8DownloaderUtil util = new M3u8DownloaderUtil(new DefaultHttpService());
        LinkedList<TsFileInfo> infoList = new LinkedList<>();

        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(TS_SAVE_TO_PATH + "play.ts"))) {
            long previousTime = 0;
            long compareTime = previousTime;
            while (true) {
                List<TsFileInfo> tsFileInfos = util.getTsFileName(M3U8_URL);

                for (int i = 0; i < tsFileInfos.size(); i++) {
                    TsFileInfo info = tsFileInfos.get(i);
                    if (previousTime < info.getTimeInMs()) {
                        infoList.add(info);
                        executorService.submit(new DownloadFile(info, util));
                        previousTime = info.getTimeInMs();
                    }
                    processInfoList(infoList, bos);
                }
                if (compareTime == previousTime) {
                    logger.info("sleep 8 seconds..");
                    Thread.sleep(8000); //avoid unnecessarily calling to download m3u8 file many times
                } else {
                    compareTime = previousTime;
                }
            }
        }
    }

    private static void processInfoList(LinkedList<TsFileInfo> infoList, OutputStream os) throws IOException {

        List<TsFileInfo> toRemove = new ArrayList<>();

        for (int i = 0; i < infoList.size(); i++) {
            TsFileInfo info = infoList.get(i);
            if (DownloadStatus.ERROR == info.getStatus()) {
                toRemove.add(info);
            } else if (DownloadStatus.INIT == info.getStatus()) {
                if ((infoList.size() - i) > SKIP_WHEN_NUM_OF_TS_READY) {
                    boolean isRemove = true;
                    for (int k = i + 1; k <= i + 3; k++) {
                        if (DownloadStatus.COMPLETED == infoList.get(k).getStatus()) {
                            continue;
                        } else {
                            isRemove = false;
                        }
                        if (isRemove) {
                            toRemove.add(info);
                        }
                    }
                }
            }
        }
        for (int i = 0; i < toRemove.size(); i++) {
            logger.info("Failed to download - filename: " + infoList.get(i).getFilename());
            infoList.remove(toRemove.get(i));
        }

        toRemove.clear();
        for (int i = 0; i < infoList.size(); i++) {
            TsFileInfo info = infoList.get(i);
            if (DownloadStatus.COMPLETED == info.getStatus()) {
                os.write(info.getByteArrayOutputStream().toByteArray());
                info.getByteArrayOutputStream().close();
                toRemove.add(info);
            } else {
                break;
            }
        }
        for (int i = 0; i < toRemove.size(); i++) {
            infoList.remove(toRemove.get(i));
        }
    }

    public static class DownloadFile implements Runnable {

        private TsFileInfo tsFileInfo;
        private M3u8DownloaderUtil util;

        public DownloadFile(TsFileInfo tsFileInfo, M3u8DownloaderUtil util) {
            this.tsFileInfo = tsFileInfo;
            this.util = util;
        }


        @Override
        public void run() {
            try {
                util.downloadFile(TS_DOWNLOAD_URL + tsFileInfo.getFilename(), tsFileInfo.getByteArrayOutputStream());
                tsFileInfo.setStatus(DownloadStatus.COMPLETED);
            } catch (Exception e) {
                tsFileInfo.setStatus(DownloadStatus.ERROR);
                logger.error(e, e);
            }
        }
    }
}
