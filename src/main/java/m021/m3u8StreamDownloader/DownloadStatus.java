package m021.m3u8StreamDownloader;

public enum DownloadStatus {
    INIT, COMPLETED, ERROR
}
