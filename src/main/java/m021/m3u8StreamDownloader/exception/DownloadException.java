package m021.m3u8StreamDownloader.exception;

public class DownloadException extends Exception {

    public DownloadException(){
        super();
    }
    public DownloadException(String message){
        super(message);
    }

    public DownloadException(String message, Throwable cause) {
        super(message, cause);
    }

}
