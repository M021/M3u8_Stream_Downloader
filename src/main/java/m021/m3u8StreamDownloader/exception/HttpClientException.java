package m021.m3u8StreamDownloader.exception;

public class HttpClientException extends Exception {
    public HttpClientException(){
        super();
    }
    public HttpClientException(String message){
        super(message);
    }

    public HttpClientException(String message, Throwable cause) {
        super(message, cause);
    }

}