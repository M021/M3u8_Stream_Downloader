package m021.m3u8StreamDownloader.exception;

public class DownloadTimeoutException extends DownloadException {
    public DownloadTimeoutException(){
        super();
    }
    public DownloadTimeoutException(String message){
        super(message);
    }

    public DownloadTimeoutException(String message, Throwable cause) {
        super(message, cause);
    }

}
